
# Join the domain

En esta practica haremos que los usuarios del OpenLDAP server puedan meterse como usuarios locales en maquinas que configuaremos como OpenLDAP clientes

# Parte 1
Primero instalamos phpldapadmin, mysql-server y slapd. Tambien tenemos que ir a github para descargarnos una version nueva “[slapd para php 8.1”](https://github.com/leenooks/phpLDAPadmin/releases/tag/1.2.6.4).

Ahora cambiamos el contenido de **/etc/phpldapadmin**

![zip_/etc](./fotos/php.png)

Y lo mismo en **/usr/share/phpldapadmin**:

![zip_/usr](./fotos/php2.png)

![fstab](./fotos/phppagina.png)


Tambien podemos despues reconfigurar.

![montado](./photos/pkg.png)

 
Despues creamos los usuarios 

![resultado](./photos/usuarios.png)


## Instalacion en el cliente

Vamos a instalar en un ubuntu cliente los paquetes libnss-ldap, libpam-ldap, ldap-utils


```
sudo apt install ldap-utils libnss-ldap libpam-ldap
```
Y despues este comando para mirar el servidor desde terminal
```
ldapsearch -x -b dc=ubuntusrv,dc=smx2023,dc=net -H slap://192.168.5.166
```

Y ya podemos mirar el contenido al servidor

