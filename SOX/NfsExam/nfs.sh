#!/bin/bash
date=`date +%H-%M`
rootcheck=`id | cut -d"(" -f3 | cut -d")" -f1`
if [ ! $rootcheck = "root"  ]; then
	echo "no tienes permisos"
	exit 1
			
fi


while true; do
	ruta="/mnt/finished"
	sleep 1
	if [ -e $ruta  ]; then
		mount 192.168.4.254:/srv/nfs/finished-on-server /mnt/finished
		echo "-------------------------------"
		echo "Tienes permisos"
		echo "Time: $date"
		echo "The mount point exists at $ruta"
		ls -1 $ruta
		echo "The cards present are:"	
		for i in `ls $ruta`; do
			num=`echo $i | cut -d"-" -f2`
			let rest=a-i
			
			if [ $rest -gt 1  ]; then
				((cont++))
				echo "$cont Gap: $rest elements"
			fi	
					
					
			a=$i
		done	
		cont=0
	else
		mkdir $ruta
		mount 192.168.4.254:/srv/nfs/finished-on-server /mnt/finished
	fi
done
