#!/bin/bash

#made by: Jose A. Losa

#Colours
green="\e[0;32m\033[1m"
end="\033[0m\e[0m"
red="\e[0;31m\033[1m"
blue="\e[0;34m\033[1m"
yellow="\e[0;33m\033[1m"
purple="\e[0;35m\033[1m"
turquoise="\e[0;36m\033[1m"
gray="\e[0;37m\033[1m"
rc=0
if [ ! $# -eq 1 ]; then
	echo -e ${red}"[*] a argument its needed"${end}
	exit 1
fi
echo $1 | grep -q "^/" || rc=1

if [ $rc -eq 0 ]; then

	echo -e ${gray}"[*]$1 is a absolute path"${end}


fi





exit 0
