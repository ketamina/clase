#!/bin/bash

#made by: Jose A. Losa

#Colours
green="\e[0;32m\033[1m"
end="\033[0m\e[0m"
red="\e[0;31m\033[1m"
blue="\e[0;34m\033[1m"
yellow="\e[0;33m\033[1m"
purple="\e[0;35m\033[1m"
turquoise="\e[0;36m\033[1m"
gray="\e[0;37m\033[1m"
check() {
permissions=`ls -l $1 | cut -c 2,3,4`
if [ -f $1 ]; then
	
	echo -e ${purple}"[*] the file exist and has this permissions $permissions"${end}
elif [ -d $1 ]; then
	echo -e ${purple}"[*] the directory exists"${end} 
fi


}
var=`echo $1 | cut -c 1 `
var2=`echo $1 | cut -c 1,2 `

if [ ! $# -eq 1 ]; then
	echo -e ${red}"[*] a argument needed"${end}
	exit 1
fi
regex="[a-z$]"
regex2="[^A-Z]*"
if [[ $var =~ $regex || $var = "." ]]; then

	echo -e ${blue}"[*] relative path"${end}
	check $1
elif [[ $var = "/" || $var2 =~ $regex2 ]]; then
	echo -e ${blue}"[*] absolute path"${end}
	check $1
fi


exit 0






exit 0
