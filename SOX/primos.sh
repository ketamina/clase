#!/bin/bash
for i in `seq 1 1000`; do
	contador=1
	divisores=0
	let numero2=$i+1
	suma=0

	while [ $contador -le $numero2 ]; do

        let operacion=$i%$contador
        if [ $operacion -eq 0 ];then
            let divisores+=1
            let suma+=$contador
        fi
        let contador+=1
    done

    let final=$i+1
    if [ $suma -eq $final ]; then
        echo $i
    fi
done
