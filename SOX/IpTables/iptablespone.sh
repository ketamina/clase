#!/bin/bash

if [ ! `whoami` = "root" ]; then
	echo "necesitas ser root"
	exit 1

fi

if [ ! -e "/bin/iptables" ]; then
	echo "no esta instalado, instalando"
	#apt install iptables
fi

rc=0
iptables -P INPUT ACCEPT || rc=1
iptables -P OUTPUT ACCEPT || rc=1
if [ $rc -eq 0  ]; then
	echo "puertos levantados"
else
	echo "fallo en el levantamieto de puertos"
	exit 1
fi

exit 0



