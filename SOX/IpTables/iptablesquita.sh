#!/bin/bash

if [ ! `whoami` = "root" ]; then
	echo "necesitas ser root"
	exit 1

fi

if [ ! -e "/bin/iptables" ]; then
	echo "no esta instalado, instalando"
	#apt install iptables
fi
rc=0
iptables -A INPUT -p tcp --dport 22 -j ACCEPT || rc=1
iptables -A OUTPUT -p tcp --dport 22 -j ACCEPT || rc=1
if [ $rc -eq 0  ]; then
	echo "el puerto 22 esta arriba"
else
	echo "fallo el puerto 22"
	exit 1
fi

rc=0
iptables -P INPUT DROP
iptables -P OUTPUT DROP
if [ $rc -eq 0  ]; then
	echo "purtos dropeados"
else
	echo "fallo en el dropeo de puertos"
	exit 1
fi

exit 0



