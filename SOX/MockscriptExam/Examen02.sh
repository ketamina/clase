#!/bin/bash

#made by: Jose A. Losa

#Colours
green="\e[0;32m\033[1m"
end="\033[0m\e[0m"
red="\e[0;31m\033[1m"
blue="\e[0;34m\033[1m"
yellow="\e[0;33m\033[1m"
purple="\e[0;35m\033[1m"
turquoise="\e[0;36m\033[1m"
gray="\e[0;37m\033[1m"


cat MOCK_DATA_EXAM.csv | tail -n1000 | cut -d, -f1,5 | while read line; do

id=`echo $line | cut -d, -f1`
genero=`echo $line | cut -d, -f2`
aa=`echo $line | cut -d\| -f3`


contador=1
divisores=0
let id2=$id+1
suma=0

    while [ $contador -le $id2 ]; do

        let operacion=$id%$contador
        if [ $operacion -eq 0 ]; then
            let divisores+=1
            let suma+=$contador
        fi
        let contador+=1
    done
 
    let final=$id+1
    if [ $suma -eq $final ]; then
        if [ -z $aa ]; then
            echo -e ${green}$line${end}
        fi
    fi


done





exit 0
