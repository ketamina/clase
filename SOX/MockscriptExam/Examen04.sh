#!/bin/bash

#made by: Jose A. Losa

#Colours
green="\e[0;32m\033[1m"
end="\033[0m\e[0m"
red="\e[0;31m\033[1m"
blue="\e[0;34m\033[1m"
yellow="\e[0;33m\033[1m"
purple="\e[0;35m\033[1m"
turquoise="\e[0;36m\033[1m"
gray="\e[0;37m\033[1m"


cat MOCK_DATA_EXAM.csv | tail -n1000 | cut -d, -f1,4,6,8 | while read line; do
    rc=0
    rb=0
    id=`echo $line | cut -d, -f1`
    per=`echo $line | cut -d, -f4`
    ip=`echo $line | cut -d, -f2,3`
    echo $ip | grep -qw $per || rc=1 
    echo $ip | grep -qw $id || rb=1 
    if [ $rc -eq 0 -o $rb -eq 0 ]; then
        echo -e ${purple}$line${end} 
    fi
done 2> /dev/null





exit 0
