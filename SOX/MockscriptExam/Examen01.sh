#!/bin/bash

#made by: Jose A. Losa

#Colours
green="\e[0;32m\033[1m"
end="\033[0m\e[0m"
red="\e[0;31m\033[1m"
blue="\e[0;34m\033[1m"
yellow="\e[0;33m\033[1m"
purple="\e[0;35m\033[1m"
turquoise="\e[0;36m\033[1m"
gray="\e[0;37m\033[1m"

cat MOCK_DATA_EXAM.csv | tail -n1000 | cut -d, -f1,2,4,7 | while read line; do

    aa=`echo $line | cut -d, -f3`
    
    bb=`ipcalc $aa | tail -n2 | head -n1 | tr -s " " | cut -d" " -f4`
    cc=`echo $line | cut -d, -f4`
    
    if [ $bb = "B" -a $cc = "Weekly" -o $cc = "Monthly" ]; then
        if [ $cc = "Weekly"  ]; then

            echo -e ${blue}$line tipo B${end}
        
        elif [ $cc = "Monthly" ]; then
            echo -e ${blue}$line tipo B${end} 

        fi
    elif [ $bb = "C" -a $cc = "Weekly" -o $cc = "Monthly" ]; then
        if [ $cc = "Weekly" ]; then
            echo -e ${purple}$line tipo C${end}
        elif [ $cc = "Monthly" ]; then
            echo -e ${purple}$line${end} tipo C 
        fi
    fi 2> /dev/null

done






exit 0
