#!/bin/bash

#made by: Jose A. Losa

#Colours
green="\e[0;32m\033[1m"
end="\033[0m\e[0m"
red="\e[0;31m\033[1m"
blue="\e[0;34m\033[1m"
yellow="\e[0;33m\033[1m"
purple="\e[0;35m\033[1m"
turquoise="\e[0;36m\033[1m"
gray="\e[0;37m\033[1m"

cat MOCK_DATA.csv | grep ".txt" | cut -d"," -f2,4 | while read line; do
    echo -e ${purple}$line${end}

done