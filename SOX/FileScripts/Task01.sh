#!/bin/bash

#made by: Jose A. Losa

#Colours
green="\e[0;32m\033[1m"
end="\033[0m\e[0m"
red="\e[0;31m\033[1m"
blue="\e[0;34m\033[1m"
yellow="\e[0;33m\033[1m"
purple="\e[0;35m\033[1m"
turquoise="\e[0;36m\033[1m"
gray="\e[0;37m\033[1m"

cat MOCK_DATA.csv | tail -n1000 | while read line; do
    numero=`echo $line | cut -d, -f1`
contador=1
divisores=0
let numero2=$numero+1
suma=0

    while [ $contador -le $numero2 ]; do

        let operacion=$numero%$contador
        if [ $operacion -eq 0 ];then
            let divisores+=1
            let suma+=$contador
        fi
        let contador+=1
    done

    let final=$numero+1
    if [ $suma -eq $final ]; then
        resultado=`echo -e $line  | cut -d, -f1,8`
        echo -e ${green}$resultado${end}
    fi


done