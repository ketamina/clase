#!/bin/bash

#made by: Jose A. Losa

#Colours
green="\e[0;32m\033[1m"
end="\033[0m\e[0m"
red="\e[0;31m\033[1m"
blue="\e[0;34m\033[1m"
yellow="\e[0;33m\033[1m"
purple="\e[0;35m\033[1m"
turquoise="\e[0;36m\033[1m"
gray="\e[0;37m\033[1m"

cat MOCK_DATA.csv | tail -n999  | while read line; do
    octeto1=`echo $line | cut -d, -f5,8 |  cut -d. -f1`
    aa=`echo $line | cut -d, -f5,8`
     
    if [ $octeto1 -eq 10 ]; then

        echo -e ${purple}$aa Tipo A${end}
    elif [ $octeto1 -eq 172 ]; then
        echo -e ${turquoise}$aa Tipo B${end}    
    fi

done