#!/bin/bash

#made by: Jose A. Losa

#Colours
green="\e[0;32m\033[1m"
end="\033[0m\e[0m"
red="\e[0;31m\033[1m"
blue="\e[0;34m\033[1m"
yellow="\e[0;33m\033[1m"
purple="\e[0;35m\033[1m"
turquoise="\e[0;36m\033[1m"
gray="\e[0;37m\033[1m"

cat MOCK_DATA.csv | tail -n1000 | while read line; do

    aa=`echo $line | cut -d, -f1`
    bb=`echo $line | cut -d, -f8 | cut -c1 | grep ^f`
    let rest1=$aa%2
    let rest2=$aa%6
    
    
    if  [ $rest1 -ge 1 -o $rest2 -eq 0 -a $bb = f ]; then
       echo -e ${purple}$line${end} | cut -d, -f1,8,9
    fi 2>/dev/null
done

exit 0 
