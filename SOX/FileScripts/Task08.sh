#!/bin/bash

#made by: Jose A. Losa

#Colours
green="\e[0;32m\033[1m"
end="\033[0m\e[0m"
red="\e[0;31m\033[1m"
blue="\e[0;34m\033[1m"
yellow="\e[0;33m\033[1m"
purple="\e[0;35m\033[1m"
turquoise="\e[0;36m\033[1m"
gray="\e[0;37m\033[1m"


for i in `seq 1000`; do
    let mod=$i%7
    if [ $mod -eq 0 ]; then
        rc=0
        aa=`cat MOCK_DATA.csv | cut -d, -f1,2 |  grep -w ^$i`
        echo $aa | cut -d, -f2 | grep -iq ^d || rc=1
        if [ $rc -eq 0 ]; then
            echo ${blue}$aa${end}
        fi
        
    
    fi
done





exit 0
